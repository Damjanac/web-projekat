'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
 
gulp.task('sass', function () {
  return gulp.src('../sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(gulp.dest('../css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('../sass/**/*.scss', ['sass']);
});

gulp.task('default', [ 'sass', 'sass:watch' ]);