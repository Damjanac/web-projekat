var app = angular.module('app', ['ngRoute']);

app.controller('Search', function($scope, $http, $rootScope, $compile, $route){
	
	$scope.searchInput = "";
	$scope.users = [];
	$scope.topics = [];
	$scope.subreddits = [];
	$scope.gotData = false;
	$scope.search = function(){
		
		$http({
	        url: "api/search/data", 
	        method: "GET",
	        params: {input: $scope.searchInput}
	    }).then(function(response) {
	    	if(response.data.error == null){
	    		console.log(response.data.response.users);
	    		$scope.users = response.data.response.users;
	    		$scope.topics = response.data.response.topics;
	    		$scope.subreddits = response.data.response.subreddit;
	    		if(!$scope.$$phase) {
            		$scope.$apply();  
        		}
	    	}
	    		
	    	else {
	    		bootbox.alert(response.data.error);
	    	}
	            	
	        });
		
	}
	
	
});

app.controller('MainCtrl', function($scope, $http, $rootScope, $compile){
	
	$scope.subscribed = [];
	
	
	$http({
        url: "api/auth/isLoggedIn", 
        method: "GET"
       
    }).then(function(response) {
            if(response.data.error == null){
            	$rootScope.loggedIn = true;
            	$rootScope.username = response.data.response.username;
            	$rootScope.role = response.data.response.role;
            	$("#block2").empty();
            	$('#block2').append($compile("<logged></logged>")($scope));
            	if(!$scope.$$phase) {
            		$scope.$apply();  
        		}
            	
            }
            	
            else{
            	$rootScope.loggedIn = false;
            	$("#block2").empty();
            	$('#block2').append($compile("<need-login></need-login>")($scope));
            	if(!$scope.$$phase) {
            		$scope.$apply();  
        		}
            	
            }
            	
        });
	
	$http({
        url: "api/subscribe/get_subscribed", 
        method: "GET"
       
    }).then(function(response) {
            if(response.data.error == null){
            	$scope.subscribed = response.data.response.subscribedTo;
            }
            	
            else{
            	console.log(response.data);
            	
            }
            	
        });
	
	
	
});

app.controller('ShowMessages', function($scope, $http, $rootScope) {
	
	$scope.messages = [];
	
	$http({
        url: "api/message/show_messages", 
        method: "GET"
    }).then(function(response) {
            if(response.data.error == null){
            	console.log(response.data);
            	$scope.messages = response.data.response.messages;
            }
            else
            	console.log(response.data.error);
        });
	
	
	
});
app.controller('Logout', function($scope, $http, $rootScope) {
	
	$scope.logout = function(){
		if($rootScope.loggedIn == true){
			$http({
	            url: "api/auth/logout", 
	            method: "GET"
	        }).then(function(response) {
	                if(response.data.error == null)
	                	location.reload();
	                else
	                	bootbox.alert(response.data.error);
	            });
			
	
			
	}
	
		
}
	
	
});

app.controller('Register', function($scope, $http, $rootScope) {
    
    $scope.name = "";
    $scope.lastname = "";
    $scope.username = "";
    $scope.password = "";
    $scope.repeatPass = "";
    $scope.email = "";
    $scope.phone = "";
    
    
   
    $scope.tryRegister = function(){
    	var passed = true;
        if($scope.name == ""){
        	bootbox.alert("Name is missing");
        	passed = false;
        }
            
        else if($scope.lastname == ""){
        	bootbox.alert("Last name is missing");
        	passed = false;
        }
            
        else if($scope.username == ""){
        	bootbox.alert("Username is missing");
        	passed = false;
        }
            
        else if($scope.email == ""){
        	bootbox.alert("Email is missing");
        	passed = false;
        }
            
        else if($scope.phone == ""){
        	bootbox.alert("Phone number is missing");
        	passed = false;
        }
            
        else if($scope.password == ""){
        	bootbox.alert("Password is missing");
        	passed = false;
        }
            
        
        if($scope.password != $scope.repeatPass){
        	bootbox.alert("Passwords do not match.");
        	passed = false;
        }
        
        if(passed){
        	$http({
                url: "api/auth/register", 
                method: "GET",
                params: {username: $scope.username, password: $scope.password, email: $scope.email, lastname: $scope.lastname, phone: $scope.phone, 
        				subs: "", role: "1", name: $scope.name}
            }).then(function(response) {
                    if(response.data.error == null){
                    	bootbox.alert({ 
                    		  message: "Account created!", 
                    		  callback: function(){ location.reload(); }
                    		});
                    	
                    }
                    	
                    else
                    	bootbox.alert(response.data.error);
                });
        }
            
    }
   
});

app.controller('SendMessage', function($scope, $http, $rootScope) {
	
	$scope.content = "";
	$scope.receiver = ""
	
	$scope.sendMessage = function(){
		console.log("ERR");
		if($scope.content == ""){
			bootbox.alert("You didn't enter the message");
			return;
		}
		else if($scope.receiver == ""){
			bootbox.alert("You didn't specify the receiver");
			return;
		}
		
		$http({
            url: "api/message/send", 
            method: "GET",
            params: {receiver: $scope.receiver, content: $scope.content}
        }).then(function(response) {
                if(response.data.error == null){   
                	bootbox.alert({ 
              		  message: "Message sent successfully!", 
              		  callback: function(){ location.reload(); }
              		});
                }
                	
                else
                	bootbox.alert(response.data.error);
            });
		
	}
	
	
});
app.controller('Login', function($scope, $http, $rootScope) {
	
	$scope.username = "";
    $scope.password = "";
    
    $scope.tryLogin = function(){
    	var passed = true;
    	if($scope.username == ""){
    		passed = false;
    		bootbox.alert("Please enter username");
    	}
    		
    	else if($scope.password == ""){
    		passed = false;
    		bootbox.alert("Please enter password");
    	}
    	
    	if(passed){
    		
    		
    		
        	$http({
                url: "api/auth/login", 
                method: "GET",
                params: {username: $scope.username, password: $scope.password}
            }).then(function(response) {
                    if(response.data.error == null){         
                    	location.reload(); 
                    }
                    	
                    else
                    	bootbox.alert(response.data.error);
                });
        }
    		
    }
	
});


app.controller('ShowSubreddits', function($scope, $http, $route) {
	
	$scope.subreddits = [];
	$scope.isModOrAdmin = false;
	$scope.isAdmin = false;
	
	$http({
        url: "api/auth/isAdmin", 
        method: "GET"
    }).then(function(response) {
            if(response.data.error != null){}
            	
            else{
            	$scope.isAdmin = true;
            	console.log("You are admin");
            	
        	}
            	
        });
	
	$http({
        url: "api/auth/isAdminOrMod", 
        method: "GET"
    }).then(function(response) {
            if(response.data.error != null){}
            	
            else{
            	$scope.isModOrAdmin = true;
            	
            	
        	}
            	
        });
	
	
	$scope.subscribe = function(subreddit_id, subreddit_name){
		$http({
	        url: "api/subscribe/execute", 
	        method: "GET",
	        params: {subreddit_id: subreddit_id, subreddit_name: subreddit_name}
	    }).then(function(response) {
	            if(response.data.error == null){
	            	bootbox.alert("Successfully subscribed!");
	            	$route.reload();
	            }
	            else{
	            	bootbox.alert(response.data.error);
	            	
	            }
	            	
	        });
		
	}
	
	$scope.tryDelete = function(subreddit_id){
		$http({
	        url: "api/subreddit/delete_subreddit", 
	        method: "GET",
	        params: {subreddit_id: subreddit_id}
	    }).then(function(response) {
	            if(response.data.error == null)
	            	$route.reload();
	            else{
	            	bootbox.alert(response.data.error);
	            	
	            }
	            	
	        });
	}
	
	
	$http({
        url: "api/subreddit/get_all_subreddits", 
        method: "GET",
        params: {username: $scope.username, password: $scope.password}
    }).then(function(response) {
            if(response.data.error == null)
            	bootbox.alert(response.data.error);
            	
            	
            else{
            	$scope.subreddits = response.data.response.subreddits;
            	
            }
            	
        });

});

app.controller('ShowSubreddit', ['$scope','$routeParams','$http', '$rootScope', '$route', function($scope, $routeParams, $http, $rootScope, $route) {
	$scope.subreddit;
	$scope.isModOrAdmin = false;
	
	
	$http({
        url: "api/auth/isAdminOrMod", 
        method: "GET"
    }).then(function(response) {
            if(response.data.error != null){}
            	
            else{
            	$scope.isModOrAdmin = true;
            	
            	
        	}
            	
        });
	
	
	$rootScope.currentSubreddit = $routeParams.id;
	$http({
        url: "api/subreddit/get_subreddit", 
        method: "GET",
        params: {id: $routeParams.id}
    }).then(function(response) {
            if(response.data.error != null)
            	bootbox.alert(response.data.error);
            else{
            	$scope.subreddit = response.data.response;
            	
        	}
            	
        });
	
	$scope.parseInt = parseInt;
	
	$scope.isTextTopic = function(type){
		if(type == "Text")
			return true;
		return false;
	}
	
	$scope.isLinkTopic = function(type){
		
		if(type == "Link")
			return true;
		return false;
	}
	
	$scope.tryDelete = function(id){
		$http({
	        url: "api/topic/delete", 
	        method: "GET",
	        params: {topic_id: id}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$route.reload();
	            } 	
	        });	
	}
	
	$scope.isImageTopic = function(type){
		
		if(type == "Image")
			return true;
		return false;
	}
	
	
	
	
	$scope.redirectToImage = function(url){
		location.href = url;
	}
	
	$scope.dislike = function(post_id){
		$http({
	        url: "api/like/topic", 
	        method: "GET",
	        params: {username: $rootScope.username, is_like: 0, post_id : post_id}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	//$route.reload();
	            } 	
	        });	
	}
	
	$scope.like = function(post_id){
		$http({
	        url: "api/like/topic", 
	        method: "GET",
	        params: {username: $rootScope.username, is_like: 1, post_id : post_id}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	//$route.reload();
	            } 	
	        });	
	}
	
	
}]);

app.controller('ShowTopic', ['$scope','$routeParams','$http','$rootScope', '$route', function($scope, $routeParams, $http, $rootScope, $route) {
	
	$rootScope.currentTopic = $routeParams.id;
	$scope.topic;
	$scope.text = "";
	$http({
        url: "api/subreddit/get_topic", 
        method: "GET",
        params: {id: $routeParams.id}
    }).then(function(response) {
            if(response.data.error != null)
            	bootbox.alert(response.data.error);
            else{
            	$scope.topic = response.data.response;
            	
            }
            	
        });
	
	$scope.dislike = function(comment_id){
		$http({
	        url: "api/like/comment", 
	        method: "GET",
	        params: {username: $rootScope.username, is_like: 0, comment_id : comment_id}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	
	            } 	
	        });	
	}
	
	$scope.like = function(comment_id){
		$http({
	        url: "api/like/comment", 
	        method: "GET",
	        params: {username: $rootScope.username, is_like: 1, comment_id : comment_id}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	
	            } 	
	        });	
	}
	
	$scope.tryCreateCommentOnTopic = function(topic_id, text){
		$http({
	        url: "api/comment/create_comment_on_topic", 
	        method: "GET",
	        params: {username: $rootScope.username, topic_id: topic_id, text : text}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$route.reload();
	            } 	
	        });	
		
		
	}
	
	$scope.canDelete = function(username){
		if($rootScope.username == username || $rootScope.role == 1 || $rootScope.role == 2)
		return true;
	}
	
	$scope.tryDelete = function(comment_id){
		$http({
	        url: "api/comment/delete_comment", 
	        method: "GET",
	        params: {comment_id: comment_id}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$route.reload();
	            } 	
	        });	
		
	}
	
	
	
	$scope.tryEdit = function(comment_id, text){
		
		$http({
	        url: "api/comment/edit_comment", 
	        method: "GET",
	        params: {comment_id: comment_id, text: text}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$route.reload();
	            } 	
	        });	
		
	};
	
	
	
}]);



app.controller('CreateTopic', ['$scope','$routeParams','$http','$rootScope', '$route', '$location', function($scope, $routeParams, $http, $rootScope, $route, $location) {
	
	$scope.title = "";
	$scope.type = "";
	$scope.content = "";
	$scope.select = "";
	$scope.text = false;
	$scope.image = false;
	$scope.link = false;
	$scope.imageUrl = "";
	$scope.linkUrl = "";
	$scope.contentToSend = "";
	
	$scope.chooseType = function(){
		$scope.image = false;
		$scope.url = false;
		$scope.text = false;
		console.log($scope.select);
		if($scope.select == "Text")
			$scope.text = true;
		else if($scope.select == "Image")
			$scope.image = true;
		else if($scope.select == "Link")
			$scope.link = true;
		
	};
	
	$scope.createTopic = function(){
		
		if($scope.image)
			$scope.contentToSend = $scope.imageUrl;
		else if($scope.link)
			$scope.contentToSend = $scope.linkUrl;
		else if($scope.text)
			$scope.contentToSend = $scope.content;
		
		
		if($scope.title == ""){
			bootbox.alert("You haven't entered title yet.");
			return;
		}
		
		if(!$scope.image && !$scope.text && !$scope.link){
			bootbox.alert("Select topic type");
			return;
		}
		
		
		$http({
	        url: "api/topic/create_topic", 
	        method: "GET",
	        params: {topic_id: $rootScope.currentSubreddit, text: $scope.contentToSend, type: $scope.select, title: $scope.title }
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$location.path('/subreddit/' + $rootScope.currentSubreddit);
	            } 	
	        });	
		
	};
		
		
	
}]);


app.controller('EditTopic', ['$scope','$routeParams','$http','$rootScope', '$route', '$location', function($scope, $routeParams, $http, $rootScope, $route, $location) {
	
	$scope.image = false;
	$scope.url = false;
	$scope.text = false;
	
	$scope.topic_id = $routeParams.topic_id;
	$scope.title = $routeParams.title;
	$scope.type = $routeParams.type;
	$scope.select = $scope.type;
	
	if($scope.select == "Text")
		$scope.text = true;
	else if($scope.select == "Image")
		$scope.image = true;
	else if($scope.select == "Link")
		$scope.link = true;
	
	$scope.chooseType = function(){
		console.log("HERE");
		$scope.image = false;
		$scope.url = false;
		$scope.text = false;
		console.log($scope.select);
		if($scope.select == "Text")
			$scope.text = true;
		else if($scope.select == "Image")
			$scope.image = true;
		else if($scope.select == "Link")
			$scope.link = true;
		
	};
	
$scope.editTopic = function(){
		
		if($scope.image)
			$scope.contentToSend = $scope.imageUrl;
		else if($scope.link)
			$scope.contentToSend = $scope.linkUrl;
		else if($scope.text)
			$scope.contentToSend = $scope.content;
		
		
		if($scope.title == ""){
			bootbox.alert("You haven't entered title yet.");
			return;
		}
		
		if(!$scope.image && !$scope.text && !$scope.link){
			bootbox.alert("Select topic type");
			return;
		}
		
		
		$http({
	        url: "api/topic/edit_topic", 
	        method: "GET",
	        params: {topic_id: $scope.topic_id,  subreddit_id: $rootScope.currentSubreddit, text: $scope.contentToSend, type: $scope.select, title: $scope.title }
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$location.path('/subreddit/' + $rootScope.currentSubreddit);
	            } 	
	        });	
		
	};
	
	
}]);



app.controller('CreateSubreddit', ['$scope','$routeParams','$http','$rootScope', '$route', '$location', function($scope, $routeParams, $http, $rootScope, $route, $location) {
	
	$scope.name = "";
	$scope.description = "";
	$scope.rules = "";
	$scope.icon = "";
	
	$scope.createSubreddit = function(){
		if($scope.name == ""){
			bootbox.alert("Name is not entered");
			return;
		}
		else if($scope.description == ""){
			bootbox.alert("There's no description");
			return;
		}
		else if($scope.rules == ""){
			bootbox.alert("There are no rules, write some");
			return;
		}
		else if($scope.icon == ""){
			bootbox.alert("Please add subreddit's icon");
			return;
		}
		
		$http({
	        url: "api/subreddit/create_subreddit", 
	        method: "POST",
	        params: {name: $scope.name, description: $scope.description, icon: $scope.icon, rules: $scope.rules}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$location.path('');
	            } 	
	        });	
			
	}
	
}]);

app.controller('ShowAllUsers', function($scope, $http, $rootScope, $route) {
	
	$scope.users;
	
	$http({
        url: "api/subreddit/show_all_users", 
        method: "GET"
    }).then(function(response) {
            if(response.data.error != null)
            	bootbox.alert(response.data.error);
            else{
            	$scope.users = response.data.response.users;
            	
            } 	
        });	
	
	$scope.editRole = function(username, role){
		$http({
	        url: "api/subreddit/edit_user_role", 
	        method: "GET",
	        params: {user_role: role, user_username: username}
	    }).then(function(response) {
	            if(response.data.error != null)
	            	bootbox.alert(response.data.error);
	            else{
	            	$route.reload();
	            	
	            } 	
	        });	
		
		
	}
});


app.directive('logged', function() {
	  return {
	    restrict: 'E',
	    templateUrl: 'logged.html'
	  };
});

app.directive('needLogin', function() {
	  return {
	    restrict: 'E',
	    templateUrl: 'need-login.html'
	  };
});

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "showAllSubreddits.html"
    })
    .when("/subreddit/:id", {
        templateUrl : "subreddit.html"
    })
    .when("/topic/:id", {
        templateUrl : "topic.html"
    })
    .when("/create_new_topic/:id", {
        templateUrl : "create-topic.html"
    })
    .when("/edit_topic/:topic_id/:title/:type", {
        templateUrl : "edit_topic.html"
    })
    .when("/create_new_subreddit/", {
        templateUrl : "create_new_subreddit.html"
    })
    .when("/show_all_users", {
        templateUrl : "show-all-users.html"
    })
    .when("/search", {
        templateUrl : "search.html"
    })
    .when("/send_message", {
        templateUrl : "send-message.html"
    })
    .when("/show_messages", {
        templateUrl : "show-messages.html"
    })
});

















/* DIRECTIVES ---------------------------------------------
*/
app.directive("fileread", [function () {
return {
    scope: {
        fileread: "="
    },
    link: function (scope, element, attributes) {
        element.bind("change", function (changeEvent) {
            var reader = new FileReader();
            reader.onload = function (loadEvent) {
                scope.$apply(function () {
                    scope.fileread = loadEvent.target.result;
                });
            }
            reader.readAsDataURL(changeEvent.target.files[0]);
        });
    }
}
}]);