Source kod od projekta


Uputstvo za pokretanje:

1. Raspakovati platform.zip
2. Otvoriti command prompt i pogledati gde je home pri pokretanju command prompta-a (to je uglavnom C:/Users/(ime korisnika); u mom slucaju je C:/Users/Luka (http://prntscr.com/fr6gi4))
3. Iz raspakovanog zipa, otici u folder db, i iz njega prebaciti reddit2.h2.db fajl u gore navedenu putanju (u mom slucaju putanja fajla kad se prebaci bice C:/Users/Luka/reddit2.h2.db)
4. Zatim iz raspakovanog zipa pronaci folder apache-tomcat-7.0.69/conf, i fajl server.xml. U njemu izmenuti jednu liniju( fajl platform/linija.md prikazuje kako ta linija treba da izgleda posle izmene)
5. Pokrenuti tomcat server i pristupiti serverskoj aplikaciji na localhost:8080/Source/
6. Prilikom restartovanja tomcata, potrebno je ponoviti korak 4, jer se fajl tad overrideuje, pa ga treba promenuti svaki put