package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.sun.xml.internal.bind.marshaller.Messages;

import beans.User;

import middlewares.Data;

public class MessageController {

	
	public static  Data sendMessage(HttpServletRequest request, String receiver, String content) throws ClassNotFoundException, SQLException{
		
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		if(receiver == null || content == null)
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		ResultSet rsa = statement.executeQuery("SELECT * FROM users WHERE username='" + receiver + "'");
		rsa.last();
		if(rsa.getRow() < 1){
			return new Data("That user does not exists", null);
		}
		
		boolean rs = statement.execute("INSERT INTO MESSAGE (sender, receiver, text, is_read) VALUES " +
				"('" + username + "', '" + receiver + "', '" + content + "', '0')");
		conn.close();
		return new Data(null, null);

	}
	
	public static  Data showMessages(HttpServletRequest request) throws ClassNotFoundException, SQLException{
		
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
	
		String username = ((User)data.getResponse()).getUsername();
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		ResultSet rsa = statement.executeQuery("SELECT * FROM message WHERE receiver='" + username + "'");
		
		conn.close();
		beans.Messages messages = new beans.Messages();
		while(rsa.next()){
			messages.addChild(new beans.Message(Integer.parseInt(rsa.getString("id")), rsa.getString("sender"), username, rsa.getString("text"), rsa.getString("is_read")));
			
		}
		
		return new Data(null, messages);
		
	}
}