package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import middlewares.Data;
import middlewares.Response;

import beans.User;

public class Auth {
	
	
	public static Data tryLogin(HttpServletRequest request, String username, String password) throws ClassNotFoundException, SQLException{
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE username = '" + username + "'" + " AND password = '" + password + "'");
		rs.last();
		conn.close();
		
		if(rs.getRow() > 0){
			
			String date = "1-1-2017";
			ArrayList<String> subscribedTo = new ArrayList<String>();
			subscribedTo.add("ELI5");
			
			
			
			User user = new User(username, password, rs.getString("email"), rs.getString("name"), rs.getString("lastname"),
						date, Integer.parseInt(rs.getString("role")), subscribedTo);
			
			Data data = new Data(null, user);
			if(!isLoggedIn(request)){
				request.getSession().setAttribute("user", data);
			}
			
			return data;
		}	
		
		return new Data("Wrong data input", null);
		
		
	}
	
	public static Data tryRegister(String username, String password, String email,
								   String firstName, String lastName, String date, int role, String subs) throws ClassNotFoundException, SQLException{
		
		
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE username = '" + username + "'" + " AND password = '" + password + "'");
		rs.last();
		conn.close();
		
		if(rs.getRow() > 0) return new Data("Username already exists!", null);
		
		boolean rsa = statement.execute("INSERT INTO users VALUES('" + password + "', '" + email + "', '" + username + "', '" + firstName + "', '" + lastName
												+ "', '0', '0', '" + subs + "')");
		
		
		conn.close();
		
		
		
		
		return new Data(null, null);
	}
	
	public static boolean isLoggedIn(HttpServletRequest request){
		
		if ((Data)request.getSession().getAttribute("user") == null)
			return false;
		
		return true;
		
	}
	
	public static boolean isAdmin(HttpServletRequest request) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		if (data == null)
			return false;
		
		String username = ((User)data.getResponse()).getUsername();
		
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE role=2 AND username='" + username + "'");
		rs.last();
		if(rs.getRow() > 0)
			return true;
		return false;
	}
	public static boolean isModOrAdmin(HttpServletRequest request) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		if (data == null)
			return false;
		
		String username = ((User)data.getResponse()).getUsername();
		
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE role=2 OR role=1 AND username='" + username + "'");
		rs.last();
		if(rs.getRow() > 0)
			return true;
		return false;
	}

}
