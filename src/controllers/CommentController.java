package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import middlewares.Data;

public class CommentController {
	
	
	
	public static Data createCommentOnTopic(HttpServletRequest request, String username, String topic_id, String text) throws ClassNotFoundException, SQLException{
		if ((Data)request.getSession().getAttribute("user") == null)
			return new Data("You are not logged in.", null);
		
		if(topic_id == null || username == null || text == null  || text.equals("")) return new Data("Wrong data input", null);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		boolean rs = statement.execute("INSERT into comments(author, date, parent, text, positive_likes, negative_likes) VALUES(" +
				"'" + username + "', '1', '" + topic_id + "' , '" + text + "', '0', '0')");
		
		
		
		conn.close();
		return null;
	}
	
	public static Data deleteComment(HttpServletRequest request, String comment_id) throws ClassNotFoundException, SQLException{
		if ((Data)request.getSession().getAttribute("user") == null)
			return new Data("You are not logged in.", null);
		
		if(comment_id == null || comment_id.equals("")) return new Data("Wrong data input", null);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		int rs = statement.executeUpdate("UPDATE comments SET is_deleted=1 WHERE id=" + comment_id);
		
		return new Data(null, null);
	}
	
	public static Data editComment(HttpServletRequest request, String comment_id, String text) throws ClassNotFoundException, SQLException{
		if ((Data)request.getSession().getAttribute("user") == null)
			return new Data("You are not logged in.", null);
		
		if(comment_id == null || comment_id.equals("")) return new Data("Wrong data input", null);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		int rs = statement.executeUpdate("UPDATE comments SET text='" + text + "' WHERE id=" + comment_id);
		System.out.println("UPDATE comments SET text='" + text + "' WHERE id=" + comment_id);
		return new Data(null, null);
	}

}
