package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import javax.servlet.http.HttpServletRequest;

import beans.Subreddits;
import beans.Topic;
import beans.User;
import beans.Users;


import middlewares.Data;
import middlewares.Response;

public class SubredditController {
	
	public static Data getAllSubreddits() throws SQLException, ClassNotFoundException{
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM subreddits");
		conn.close();
		Subreddits subreddits = new Subreddits();
		while (rs.next()) {
			
			beans.Subreddit subreddit = new beans.Subreddit(Integer.parseInt(rs.getString("id")), rs.getString("name"), rs.getString("description"), 
									rs.getString("icon"), rs.getString("rules"), rs.getString("moderator"), rs.getString("moderators"));
			
			subreddits.addChild(subreddit);
			
	    }
		
		Data data = new Data("null", subreddits);
		
		return data;
	}
	
	public static Data getSubreddit(String id) throws ClassNotFoundException, SQLException{
		if(id == null) return new Data("That subreddit does not exist.", null);
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM subreddits WHERE id='" + Integer.parseInt(id) + "'");
		rs.last();
		if(rs.getRow() < 0)
			return new Data("That subreddit does not exist.", null);
		
		beans.Subreddit  subreddit = new beans.Subreddit(Integer.parseInt(rs.getString("id")), rs.getString("name"), rs.getString("description"), rs.getString("icon"),
									rs.getString("rules"), rs.getString("moderator"), rs.getString("moderators"));
		
		Data data;
		
		
		rs = statement.executeQuery("SELECT * FROM topics WHERE subreddit='" + Integer.parseInt(id) + "'");
		
		conn.close();
		
		while(rs.next()){
			
			subreddit.addChild(new Topic(Integer.parseInt(rs.getString("id")), rs.getString("name"), rs.getString("author"), rs.getString("positive_comments"),
								rs.getString("negative_comments"), rs.getString("type"), rs.getString("content")));
		}
			data = new Data(null, subreddit);
			return data;
	}
	
	
	public static Data createSubreddit(HttpServletRequest request, String name, String description, String icon, String rules) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(name == null || description == null || icon == null || rules == null)
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date datet = new Date();
		String date = dateFormat.format(datet);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		
		ResultSet rsa = statement.executeQuery("SELECT * FROM subreddits WHERE name='" + name + "'");
		rsa.last();
		if(rsa.getRow() > 0)
			return new Data("Subreddit with given name already exists!", null);
			
		System.out.println("INSERT INTO subreddits(name, description, icon, rules) VALUES ('" + name + "', '" + description + "', '" + icon + "', '" + rules + "')");
		boolean rs = statement.execute("INSERT INTO subreddits(name, description, icon, rules) VALUES ('" + name + "', '" + description + "', '" + icon + "', '" + rules + "')");

		return new Data(null, null);
		
		
	}
	
	
	public static Data deleteSubreddit(HttpServletRequest request, String subreddit_id) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(subreddit_id == null)
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date datet = new Date();
		String date = dateFormat.format(datet);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		
		boolean rsa = statement.execute("DELETE FROM subreddits WHERE id=" + subreddit_id);
		
		return new Data(null, null);
		
		
	}
	
	public static Data showAllUsers(HttpServletRequest request) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(!Auth.isAdmin(request))
			return new Data("You cannot access this page", null);
		
		String username = ((User)data.getResponse()).getUsername();
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM USERS WHERE username !='" + username + "'");
		
		Users users = new Users();
		
		while(rs.next()){
			
			users.addChild(new User(rs.getString("username"), rs.getString("password"), rs.getString("role")));
			
		}
		
		return new Data(null, users);
	}
	
	public static Data editUserRole(HttpServletRequest request, String user_role, String user_username) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(!Auth.isAdmin(request))
			return new Data("You cannot access this page", null);
		
		String username = ((User)data.getResponse()).getUsername();
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		System.out.println("UPDATE users SET role=" + user_role + " WHERE username='" + user_username + "'");
		boolean rs = statement.execute("UPDATE users SET role=" + user_role + " WHERE username='" + user_username + "'");
		
		
		return new Data(null, null);
	}
	
}
