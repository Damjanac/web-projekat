package controllers;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {
	
	
	public static Connection connect() throws ClassNotFoundException, SQLException{
		
		Connection conn;
		Class.forName("org.h2.Driver");
		conn = DriverManager.getConnection("jdbc:h2:~/reddit2", "sa", "");
		return conn;						
		
	}

}
