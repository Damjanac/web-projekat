package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import middlewares.Data;

public class LikeController {
	
	
	public static  Data tryLikeTopic(HttpServletRequest request, String topic_id, String username, String isLike) throws ClassNotFoundException, SQLException{
		if ((Data)request.getSession().getAttribute("user") == null)
			return new Data("You are not logged in.", null);
		if(topic_id == null || username == null || isLike == null) return new Data("Wrong data input.", null);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM post_votes WHERE post_id='" + Integer.parseInt(topic_id) + "' AND username='" + username + "'");
		
		conn.close();
		rs.last();
		if(rs.getRow() > 0){
			return new Data("You have already liked this topic", null);
		}
		
		int liked = Integer.parseInt(isLike);
		
		if(liked == 1){
			statement.executeUpdate("UPDATE topics SET positive_comments = positive_comments + 1 WHERE id=" + topic_id);
			statement.executeUpdate("INSERT INTO post_votes(username, post_id) VALUES ('" + username + "', '" + topic_id + "')");
			
		}
		else if(liked == 0){
			statement.executeUpdate("UPDATE topics SET negative_comments = negative_comments + 1 WHERE id=" + topic_id);
			statement.executeUpdate("INSERT INTO post_votes(username, post_id) VALUES ('" + username + "', '" + topic_id + "')");
		}
		
		return new Data(null, null);
	}
	
	public static  Data tryLikeComment(HttpServletRequest request, String comment_id, String username, String isLike) throws ClassNotFoundException, SQLException{
		if ((Data)request.getSession().getAttribute("user") == null)
			return new Data("You are not logged in.", null);
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM comment_votes WHERE comment_id='" + Integer.parseInt(comment_id) + "' AND username='" + username + "'");
		
		conn.close();
		rs.last();
		if(rs.getRow() > 0){
			return new Data("You have already liked this comment", null);
		}
		
		int liked = Integer.parseInt(isLike);
		
		if(liked == 1){
			statement.executeUpdate("UPDATE comments SET positive_likes = positive_likes + 1 WHERE id=" + comment_id);
			statement.executeUpdate("INSERT INTO comment_votes(username, comment_id) VALUES ('" + username + "', '" + comment_id + "')");
			
			
		}
		else if(liked == 0){
			statement.executeUpdate("UPDATE comments SET negative_likes = negative_likes + 1 WHERE id=" + comment_id);
			statement.executeUpdate("INSERT INTO comment_votes(username, comment_id) VALUES ('" + username + "', '" + comment_id + "')");
			
		}
		
		return new Data(null, null);
	}
	
	

}
