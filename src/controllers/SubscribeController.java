package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import beans.User;

import middlewares.Data;

public class SubscribeController {

	
	public static Data subscribe(HttpServletRequest request, String subreddit_id, String subreddit_name) throws SQLException, ClassNotFoundException{
		
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(subreddit_id == null || subreddit_name == null)
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();

		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		ResultSet rs = statement.executeQuery("SELECT * FROM subscribed WHERE username='" + username + "' AND subreddit='" + subreddit_id + "'");
		rs.last();
		if(rs.getRow() > 0)
			return new Data("You have already subscribed to this subreddit", null);
		
		boolean rsa = statement.execute("INSERT INTO subscribed (username, subreddit,subreddit_name) VALUES ('" + username + "', '" + subreddit_id + "'," +
				" '" + subreddit_name + "')");
		
		return new Data(null, null);
	}
	
public static Data getSubscribed(HttpServletRequest request) throws SQLException, ClassNotFoundException{
		
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
			
		String username = ((User)data.getResponse()).getUsername();

		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		
		ResultSet rsa = statement.executeQuery("SELECT * FROM subscribed WHERE username='" + username + "'");
		ResultSet rsa2;
		ArrayList<String> subs = new ArrayList<String>();
		while(rsa.next()){
			subs.add(rsa.getString("subreddit_name"));
			
		}
		
		
		return new Data(null, new User(username, subs));
	}
}
