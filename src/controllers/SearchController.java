package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Search;
import beans.Subreddit;
import beans.Topic;
import beans.User;

import middlewares.Data;

public class SearchController {
	
	public static Data getResult(String text) throws ClassNotFoundException, SQLException{
		
		if(text == null) return new Data("Wrong data input", null);
		Search result = new Search();
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		ResultSet rs;
		String[] splitted = text.split("OR");
		for (String statements : splitted) {
			if(statements.indexOf("subreddit=") >= 0){
				String subreddit_search = statements.split("=")[1].trim();
				rs = statement.executeQuery("SELECT * FROM subreddits WHERE name='" + subreddit_search + "' OR description='" + subreddit_search + "'" +
						" OR moderator='" + subreddit_search + "'");
				while(rs.next()){
					Subreddit  subreddit = new Subreddit(Integer.parseInt(rs.getString("id")), rs.getString("name"), rs.getString("description"), rs.getString("icon"),
							rs.getString("rules"), rs.getString("moderator"), rs.getString("moderators"));
					
					result.addSubreddit(subreddit);
					
				}
			}
			if(statements.indexOf("user=") >= 0){			
				String user_search = statements.split("=")[1].trim();
				rs = statement.executeQuery("SELECT * FROM users WHERE username='" + user_search + "'");
				while(rs.next()){
					String date = "1-1-2017";
					ArrayList<String> subscribedTo = new ArrayList<String>();
					subscribedTo.add("ELI5");
					User user = new User(rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getString("name"), rs.getString("lastname"),
								"", Integer.parseInt(rs.getString("role")), subscribedTo);
					result.addUser(user);
				}
			}
			
			if(statements.indexOf("topic=") >= 0){
				String topic_search = statements.split("=")[1].trim();
				rs = statement.executeQuery("SELECT * FROM topics WHERE name='" + topic_search + "' OR content='" + topic_search + "' " +
						"OR author='" +  topic_search + "'");
				while(rs.next()){
					Topic topic = new Topic(Integer.parseInt(rs.getString("id")), rs.getString("name"), rs.getString("type"), rs.getString("author"), rs.getString("content"),
							rs.getString("date"), rs.getString("positive_comments"), rs.getString("negative_comments"), Integer.parseInt(rs.getString("subreddit")));
					result.addTopic(topic);
				}
				
			}
				
		}
		return new Data(null, result);
	}

}
