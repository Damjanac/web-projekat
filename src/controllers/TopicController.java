package controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import middlewares.Data;
import beans.Comment;
import beans.Topic;
import beans.User;

public class TopicController {
	
	
	public static Data getTopic(String id) throws ClassNotFoundException, SQLException{
		if(id == null) return new Data("That topic does not exist.", null);
			Connection conn = DBConnection.connect();
			Statement statement = DBConnection.connect().createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM topics WHERE id='" + Integer.parseInt(id) + "'");
			
			conn.close();
			rs.last();
			if(rs.getRow() < 1) new Data("That topic does not exist.", null);
			
			beans.Topic topic = new Topic(Integer.parseInt(id), rs.getString("name"), rs.getString("type"), rs.getString("author"), rs.getString("content"),
					rs.getString("date"), rs.getString("positive_comments"), rs.getString("negative_comments"), Integer.parseInt(rs.getString("subreddit")));
		
			
			rs = statement.executeQuery("SELECT * FROM comments WHERE parent='" + Integer.parseInt(id) + "' AND is_deleted=0");
			while(rs.next()){
				
				topic.addChild(new Comment(Integer.parseInt(rs.getString("id")), rs.getString("author"), rs.getString("date"), rs.getString("text"),
									rs.getString("negative_likes"), rs.getString("positive_likes")));
			}
			
			return new Data(null, topic);
			
	}

	
	public static Data createTopic(HttpServletRequest request, String subreddit_id, String text, String name, String type) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(subreddit_id == null || subreddit_id == "" || text == "" || text == null || name == "" || name == null || type == null || type == "")
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date datet = new Date();
		String date = dateFormat.format(datet);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		System.out.println("INSERT INTO topics(author, type, content, date, positive_comments, negative_comments, subreddit, name)" +
				" VALUES('" + username + "', '" + type + "', '" + text + "', '" + date + "', '0', '0', " + subreddit_id + ", '" + name +  "')");
		boolean rs = statement.execute("INSERT INTO topics(author, type, content, date, positive_comments, negative_comments, subreddit, name)" +
				" VALUES('" + username + "', '" + type + "', '" + text + "', '" + date + "', '0', '0', " + subreddit_id + ", '" + name +  "')");
		
		conn.close();
		
		
		return new Data(null, null);
	}
	
	public static Data editTopic(HttpServletRequest request, String subreddit_id, String text, String name, String type, String topic_id) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(subreddit_id == null || subreddit_id == "" || text == "" || text == null || name == "" || name == null || type == null || type == "")
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date datet = new Date();
		String date = dateFormat.format(datet);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		System.out.println("UPDATE topics SET type = '" + type  + "', date = '" + date + "', name = '" + name  + "', content = '" + text + "'" +
				" WHERE id = " + topic_id);
		boolean rs = statement.execute("UPDATE topics SET type = '" + type  + "', date = '" + date + "', name = '" + name  + "', content = '" + text + "'" +
				" WHERE id = " + topic_id);
		
		conn.close();
		
		
		return new Data(null, null);
	}
	
	public static Data deleteTopic(HttpServletRequest request, String topic_id) throws ClassNotFoundException, SQLException{
		Data data = (Data)request.getSession().getAttribute("user");
		
		if (data == null)
			return new Data("You are not logged in.", null);
		
		if(topic_id == null)
			return new Data("Wrong data input", null);
			
		String username = ((User)data.getResponse()).getUsername();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date datet = new Date();
		String date = dateFormat.format(datet);
		
		Connection conn = DBConnection.connect();
		Statement statement = DBConnection.connect().createStatement();
		
		boolean rs = statement.execute("DELETE FROM topics WHERE id=" + topic_id);
		
		conn.close();
		
		
		return new Data(null, null);
	}
	
	

}
