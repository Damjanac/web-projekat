package middlewares;

public class Data {

	private String error;
	private Response response;
	
	public Data(String error, Response response) {
		super();
		this.error = error;
		this.response = response;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
	
	
}
