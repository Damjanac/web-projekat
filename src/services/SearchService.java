package services;


import java.sql.SQLException;



import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Topic;

import middlewares.Data;
import controllers.CommentController;
import controllers.SearchController;
import controllers.SubredditController;
import controllers.TopicController;


@Path("/search")
public class SearchService {
	
	@GET
	@Path("/data")
	@Produces(MediaType.APPLICATION_JSON)
	public Data createCommentOnTopic(@QueryParam("input") String text) throws ClassNotFoundException, SQLException {
		return SearchController.getResult(text);
	}
	
}
