package services;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

import middlewares.Data;

import org.h2.Driver;

import controllers.Auth;

import beans.User;

import sun.security.provider.MD5;

@Path("/auth")
public class LoginService {
	
	@GET
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Data auth(@Context HttpServletRequest request, @QueryParam("username") String username, @QueryParam("password") String password) throws ClassNotFoundException, SQLException {
		return Auth.tryLogin(request, username, password);	
	}
	
	
	@GET
	@Path("/isLoggedIn")
	@Produces(MediaType.APPLICATION_JSON)
	public Data isLoggedIn(@Context HttpServletRequest request){
		if(Auth.isLoggedIn(request))
			return ((Data)request.getSession().getAttribute("user"));
			
			
		return new Data("You are not logged in", null);
		
	}
	
	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Data logout(@Context HttpServletRequest request){
		if(Auth.isLoggedIn(request)){
			request.getSession().invalidate();
			return new Data(null, null);
		}
		
		return new Data("You are not logged in", null);
			
	}
	
	@GET
	@Path("/register")
	@Produces(MediaType.APPLICATION_JSON)
	public Data register(@QueryParam("username") String username, @QueryParam("password") String password,@QueryParam("email") String email,
			@QueryParam("name") String name, @QueryParam("lastname") String lastname, @QueryParam("date") String date, @QueryParam("role") String role,
			@QueryParam("subs") String subs) throws NumberFormatException, ClassNotFoundException, SQLException{
		return Auth.tryRegister(username, password, email, name, lastname, date, Integer.parseInt(role), subs);
	}
	
	@GET
	@Path("/isAdmin")
	@Produces(MediaType.APPLICATION_JSON)
	public Data isAdmin(@Context HttpServletRequest request) throws ClassNotFoundException, SQLException{
		if(Auth.isAdmin(request))
			return new Data(null, null);
			
			
		return new Data("You are not administrator!", null);
		
	}
	
	@GET
	@Path("/isAdminOrMod")
	@Produces(MediaType.APPLICATION_JSON)
	public Data isAdminOrMod(@Context HttpServletRequest request) throws ClassNotFoundException, SQLException{
		if(Auth.isModOrAdmin(request))
			return new Data(null, null);
			
			
		return new Data("You are not administrator or mod!", null);
		
	}
	

}
