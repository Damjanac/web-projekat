package services;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import middlewares.Data;
import controllers.CommentController;
import controllers.LikeController;

@Path("/comment")
public class CommentService {

	
	@GET
	@Path("/create_comment_on_topic")
	@Produces(MediaType.APPLICATION_JSON)
	public Data createCommentOnTopic(@Context HttpServletRequest request, @QueryParam("username") String username, @QueryParam("topic_id") String topic_id,
			@QueryParam("text") String text) throws ClassNotFoundException, SQLException {
		System.out.println(text);
		return CommentController.createCommentOnTopic(request, username, topic_id, text);
	}
	
	@GET
	@Path("/delete_comment")
	@Produces(MediaType.APPLICATION_JSON)
	public Data deleteComment(@Context HttpServletRequest request,@QueryParam("comment_id") String comment_id) throws ClassNotFoundException, SQLException {
		return CommentController.deleteComment(request, comment_id);
	}
	
	@GET
	@Path("/edit_comment")
	@Produces(MediaType.APPLICATION_JSON)
	public Data editComment(@Context HttpServletRequest request,@QueryParam("comment_id") String comment_id,@QueryParam("text") String text) throws ClassNotFoundException, SQLException {
		return CommentController.editComment(request, comment_id, text);
	}
	
}
