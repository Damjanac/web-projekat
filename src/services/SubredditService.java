package services;


import java.sql.SQLException;



import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Topic;

import middlewares.Data;
import controllers.SubredditController;
import controllers.TopicController;


@Path("/subreddit")
public class SubredditService {
	
	
	@GET
	@Path("/get_all_subreddits")
	@Produces(MediaType.APPLICATION_JSON)
	public Data getAllSubreddits() throws ClassNotFoundException, SQLException{
		return SubredditController.getAllSubreddits();	
	}
	
	@GET
	@Path("/get_subreddit")
	@Produces(MediaType.APPLICATION_JSON)
	public Data getSubreddit(@QueryParam("id") String id) throws ClassNotFoundException, SQLException{
		return SubredditController.getSubreddit(id);	
	}
	
	@GET
	@Path("/get_topic")
	@Produces(MediaType.APPLICATION_JSON)
	public Data getTopic(@QueryParam("id") String id) throws ClassNotFoundException, SQLException{
		return TopicController.getTopic(id);	
	}
	
	@POST
	@Path("/create_subreddit")
	@Produces(MediaType.APPLICATION_JSON)
	public Data createSubreddit(@Context HttpServletRequest request,@QueryParam("name") String name, @QueryParam("description") String description,
			@QueryParam("icon") String icon, @QueryParam("rules") String rules) throws ClassNotFoundException, SQLException{
		return SubredditController.createSubreddit(request, name, description, icon, rules);	
	}
	
	@GET
	@Path("/delete_subreddit")
	@Produces(MediaType.APPLICATION_JSON)
	public Data createSubreddit(@Context HttpServletRequest request,@QueryParam("subreddit_id") String subreddit_id) throws ClassNotFoundException, SQLException{
		return SubredditController.deleteSubreddit(request, subreddit_id);
	}
	
	@GET
	@Path("/show_all_users")
	@Produces(MediaType.APPLICATION_JSON)
	public Data showAllUsers(@Context HttpServletRequest request) throws ClassNotFoundException, SQLException{
		return SubredditController.showAllUsers(request);
	}
	
	@GET
	@Path("/edit_user_role")
	@Produces(MediaType.APPLICATION_JSON)
	public Data showAllUsers(@Context HttpServletRequest request, @QueryParam("user_role") String user_role,
			@QueryParam("user_username") String user_username) throws ClassNotFoundException, SQLException{
		return SubredditController.editUserRole(request, user_role, user_username);
	}
	

}
