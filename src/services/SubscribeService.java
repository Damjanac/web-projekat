package services;

import java.sql.SQLException;



import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Topic;

import middlewares.Data;
import controllers.CommentController;
import controllers.MessageController;
import controllers.SearchController;
import controllers.SubredditController;
import controllers.SubscribeController;
import controllers.TopicController;


@Path("/subscribe")
public class SubscribeService {
	
	@GET
	@Path("/execute")
	@Produces(MediaType.APPLICATION_JSON)
	public Data subscribe(@Context HttpServletRequest request, @QueryParam("subreddit_id") String subreddit_id, @QueryParam("subreddit_name") String subreddit_name) throws ClassNotFoundException, SQLException {
		return SubscribeController.subscribe(request, subreddit_id, subreddit_name);
	}
	@GET
	@Path("/get_subscribed")
	@Produces(MediaType.APPLICATION_JSON)
	public Data getSubscribed(@Context HttpServletRequest request) throws ClassNotFoundException, SQLException {
		return SubscribeController.getSubscribed(request);
	}
}
	