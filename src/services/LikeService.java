package services;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

import middlewares.Data;

import org.h2.Driver;
import controllers.LikeController;
import controllers.Auth;

import beans.User;

import sun.security.provider.MD5;

@Path("/like")
public class LikeService {

	
	@GET
	@Path("/topic")
	@Produces(MediaType.APPLICATION_JSON)
	public Data likeTopic(@Context HttpServletRequest request, @QueryParam("post_id") String post_id, @QueryParam("username") String username,
			@QueryParam("is_like") String isLike) throws ClassNotFoundException, SQLException {
		
		return LikeController.tryLikeTopic(request, post_id, username, isLike);
	}
	
	@GET
	@Path("/comment")
	@Produces(MediaType.APPLICATION_JSON)
	public Data likeComment(@Context HttpServletRequest request, @QueryParam("comment_id") String post_id, @QueryParam("username") String username,
			@QueryParam("is_like") String isLike) throws ClassNotFoundException, SQLException {
		
		return LikeController.tryLikeComment(request, post_id, username, isLike);
	}
	
	
}
