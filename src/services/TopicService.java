package services;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import middlewares.Data;
import controllers.TopicController;

@Path("/topic")
public class TopicService {

	
	@GET
	@Path("/create_topic")
	@Produces(MediaType.APPLICATION_JSON)
	public Data createTopic(@Context HttpServletRequest request, @QueryParam("topic_id") String topic_id, @QueryParam("text") String text,
			@QueryParam("type") String type, @QueryParam("title") String title) throws ClassNotFoundException, SQLException {
		
		return TopicController.createTopic(request, topic_id, text , title, type);
	}
	

	@GET
	@Path("/edit_topic")
	@Produces(MediaType.APPLICATION_JSON)
	public Data editTopic(@Context HttpServletRequest request, @QueryParam("topic_id") String topic_id, @QueryParam("text") String text,
			@QueryParam("type") String type, @QueryParam("title") String title, @QueryParam("subreddit_id") String subreddit_id) throws ClassNotFoundException, SQLException {
		
		return TopicController.editTopic(request, subreddit_id, text , title, type, topic_id);
	}
	
	@GET
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Data editTopic(@Context HttpServletRequest request, @QueryParam("topic_id") String topic_id) throws ClassNotFoundException, SQLException {
		
		return TopicController.deleteTopic(request, topic_id);
	}
	
	
	
	
	
}
