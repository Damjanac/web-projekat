package services;

import java.sql.SQLException;



import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Topic;

import middlewares.Data;
import controllers.CommentController;
import controllers.MessageController;
import controllers.SearchController;
import controllers.SubredditController;
import controllers.TopicController;


@Path("/message")
public class MessageService {
	
	@GET
	@Path("/send")
	@Produces(MediaType.APPLICATION_JSON)
	public Data sendMessage(@Context HttpServletRequest request, @QueryParam("receiver") String receiver, @QueryParam("content") String content ) throws ClassNotFoundException, SQLException {
		return MessageController.sendMessage(request, receiver, content);
	}
	
	@GET
	@Path("/show_messages")
	@Produces(MediaType.APPLICATION_JSON)
	public Data sendMessage(@Context HttpServletRequest request) throws ClassNotFoundException, SQLException {
		return MessageController.showMessages(request);
	}
	
}
