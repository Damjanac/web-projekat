package beans;

import java.util.ArrayList;


import middlewares.Response;

public class Subreddit implements Response {
	
	private int id;
	private String name;
	private String description;
	private String icon;
	private String rules;
	private String moderator;
	private String moderators;
	private ArrayList<Topic> topics;
	
	public void addChild(Topic top){
		for (Topic topic : topics) {
			if(topic.getId() == top.getId())
				return;
		}
		topics.add(top);
	}
	
	public void getChild(int index){
		topics.get(index);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getRules() {
		return rules;
	}
	public void setRules(String rules) {
		this.rules = rules;
	}
	public String getModerator() {
		return moderator;
	}
	public void setModerator(String moderator) {
		this.moderator = moderator;
	}
	public String getModerators() {
		return moderators;
	}
	public void setModerators(String moderators) {
		this.moderators = moderators;
	}
	
	public Subreddit(int id, String name, String description, String icon,
			String rules, String moderator, String moderators) {
		super();
		this.topics = new ArrayList<Topic>();
		this.id = id;
		this.name = name;
		this.description = description;
		this.icon = icon;
		this.rules = rules;
		this.moderator = moderator;
		this.moderators = moderators;
	}

	public ArrayList<Topic> getTopics() {
		return topics;
	}

	public void setTopics(ArrayList<Topic> topics) {
		this.topics = topics;
	}
	
	public Topic getChildWithId(int id){
		for (Topic topic : topics) {
			if(topic.getId() == id){
				return topic;
			}
			
		}
		
		return null;
	}
	
	
	
	
}
