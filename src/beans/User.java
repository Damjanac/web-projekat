package beans;
import java.util.ArrayList;

import middlewares.Response;


public class User implements Response {
	
	public String username;
	private String password;
	private String email;
	private String firstName;
	private String lastname;
	private String dateCreated;
	private int role;
	private ArrayList<String> subscribedTo;
	
	public User(String username, String password, String role) {
		super();
		this.username = username;
		this.role = Integer.parseInt(role);
	}
	
	public User(String username, ArrayList<String> subscribedTo){
		this.username = username;
		this.subscribedTo = subscribedTo;
	}

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public User(String username, String password, String email,
			String firstName, String lastname, String dateCreated, int role,
			ArrayList<String> subscribedTo) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastname = lastname;
		this.dateCreated = dateCreated;
		this.role = role;
		this.subscribedTo = new ArrayList<String>();
		this.subscribedTo = subscribedTo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public ArrayList<String> getSubscribedTo() {
		return subscribedTo;
	}

	public void setSubscribedTo(ArrayList<String> subscribedTo) {
		this.subscribedTo = subscribedTo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private String getPassword() {
		return password;
	}

	private void setPassword(String password) {
		this.password = password;
	}

	
}
