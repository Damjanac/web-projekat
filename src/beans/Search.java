package beans;

import java.util.ArrayList;

import middlewares.Response;

public class Search implements Response {
	
	private ArrayList<User> users;
	private ArrayList<Topic> topics;
	private ArrayList<Subreddit> subreddits;
	
	public ArrayList<User> getUsers() {
		return users;
	}
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	public ArrayList<Topic> getTopics() {
		return topics;
	}
	public void setTopics(ArrayList<Topic> topics) {
		this.topics = topics;
	}
	public ArrayList<Subreddit> getSubreddit() {
		return subreddits;
	}
	public void setSubreddit(ArrayList<Subreddit> subreddit) {
		this.subreddits = subreddit;
	}
	
	public void addUser(User user){
		users.add(user);
	}
	public void addTopic(Topic topic){
		topics.add(topic);
	}
	
	public void addSubreddit(Subreddit subreddit){
		subreddits.add(subreddit);
	}
	
	public  Search(){
		users = new ArrayList<User>();
		topics = new ArrayList<Topic>();
		subreddits = new ArrayList<Subreddit>();
	}

}
