package beans;

import java.util.ArrayList;

import middlewares.Response;

public class Subreddits implements Response {
	
	private ArrayList<Subreddit> subreddits;
	

	public Subreddit getChild(int index){
		return subreddits.get(index);
	}
	
	public void addChild(Subreddit sub){
		for (Subreddit subreddit : subreddits) {
			if(subreddit.getId() == sub.getId())
				return;
		}
		subreddits.add(sub);
	}
	
	public Subreddit getChildWithId(int id){
		for (Subreddit subreddit : subreddits) {
			if(subreddit.getId() == id){
				return subreddit;
			}
			
		}
		
		return null;
	}
	
	
	public Subreddits(){
		
		subreddits = new ArrayList<Subreddit>();
	}

	public ArrayList<Subreddit> getSubreddits() {
		return subreddits;
	}

	public void setSubreddits(ArrayList<Subreddit> subreddits) {
		this.subreddits = subreddits;
	}
	
	

}
