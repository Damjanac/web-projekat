package beans;

import java.util.ArrayList;

public class Comment {
	
	private int id;
	private String author;
	private String date;
	private int parent;
	private ArrayList<Comment> comments;
	private String text;
	private int positive_likes;
	private int negative_likes;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getParent() {
		return parent;
	}
	public void setParent(int parent) {
		this.parent = parent;
	}
	public ArrayList<Comment> getComments() {
		return comments;
	}
	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getPositive_likes() {
		return positive_likes;
	}
	public void setPositive_likes(int positiveLikes) {
		positive_likes = positiveLikes;
	}
	public int getNegative_likes() {
		return negative_likes;
	}
	public void setNegative_likes(int negativeLikes) {
		negative_likes = negativeLikes;
	}
	public Comment(int id, String author, String date, int parent,
			ArrayList<Comment> comments, String text, int positiveLikes,
			int negativeLikes) {
		super();
		this.id = id;
		this.author = author;
		this.date = date;
		this.parent = parent;
		this.comments = comments;
		this.text = text;
		this.comments = new ArrayList<Comment>();
		positive_likes = positiveLikes;
		negative_likes = negativeLikes;
	}
	public Comment(int id, String author, String date, String text, String negative_comments, String positive_comments) {
		super();
		this.id = id;
		if(negative_comments == null)
			this.negative_likes = 0;
		else
			this.negative_likes = Integer.parseInt(negative_comments);	
		
		if(positive_comments == null)
			this.positive_likes = 0;
		else
			this.positive_likes = Integer.parseInt(positive_comments);
		
		this.author = author;
		this.date = date;
		this.text = text;
		this.comments = new ArrayList<Comment>();
	}
	
	

}
