package beans;

import java.util.ArrayList;

import middlewares.Response;

public class Users implements Response {
	private ArrayList<User> users;
	

	public User getChild(int index){
		return users.get(index);
	}
	
	public void addChild(User sub){
		for (User user : users) {
			if(user.getUsername() == sub.getUsername())
				return;
		}
		users.add(sub);
	}
	
	public User getChildWithUnsername(String Username){
		for (User user : users) {
			if(user.getUsername().equals(Username)){
				return user;
			}
			
		}
		
		return null;
	}
	
	
	public Users(){
		
		users = new ArrayList<User>();
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

}
