package beans;

import java.util.ArrayList;

import middlewares.Response;

public class Topic implements Response {
	
	private int id;
	private String name;
	private String type;
	private String author;
	private String content;
	private String date;
	private String positive_comments;
	private String negative_comments;
	private int subreddit;
	private ArrayList<Comment> comments;
	
	public Comment getChild(int index){
		return comments.get(index);
	}
	public void addChild(Comment comment){
		comments.add(comment);
	}
	
	
	public ArrayList<Comment> getComments() {
		return comments;
	}
	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPositive_comments() {
		return positive_comments;
	}
	public void setPositive_comments(String positiveComments) {
		positive_comments = positiveComments;
	}
	public String getNegative_comments() {
		return negative_comments;
	}
	public void setNegative_comments(String negativeComments) {
		negative_comments = negativeComments;
	}
	public int getSubreddit() {
		return subreddit;
	}
	public void setSubreddit(int subreddit) {
		this.subreddit = subreddit;
	}
	
	public Topic(int id, String name, String type, String author,
			String content, String date, String positiveComments,
			String negativeComments, int subreddit) {
		super();
		this.comments = new ArrayList<Comment>();
		this.id = id;
		this.name = name;
		this.type = type;
		this.author = author;
		this.content = content;
		this.date = date;
		positive_comments = positiveComments;
		negative_comments = negativeComments;
		this.subreddit = subreddit;
	}
	
	public Topic(int id, String name, String author, String positiveComments, String negativeComments, String type, String content) {
		super();
		this.author = author;
		this.content = content;
		this.positive_comments = positiveComments;
		this.negative_comments = negativeComments;
		this.type = type;
		this.id = id;
		this.name = name;
	}
	
}
