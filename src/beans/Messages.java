package beans;

import java.util.ArrayList;

import middlewares.Response;

public class Messages implements Response {
	
	private ArrayList<Message> messages;

	public ArrayList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}
	
	public void addChild(Message message){
		messages.add(message);
	}

	
	public Messages(){
		messages = new ArrayList<Message>();
	}
}
